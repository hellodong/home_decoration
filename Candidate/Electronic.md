## 大件电器

  

### 油烟机、燃气灶

  

#### 华帝油烟机燃气灶(**RMB 4150**):

  

![](./images/vatti_youyanji_ranqizao.jpg)

  

油烟机参数:

  

![](./images/vatti_S12_youyanji_parameter.jpg)

  

油烟机安装:

  

![](./images/vatti_youyanji_install.jpg)

  

燃气灶参数:

  

![](./images/vatti_ranqizao_i10079B_parameter.jpg)

  
  

#### 老板油烟机燃气灶(**RMB 4060**):

  

![](./images/roban_youyanji_ranqizao.jpg)

  

油烟机尺寸:

  

![](./images/roban_youyanji_size.jpg)

  

油烟机安装尺寸:

  

![](./images/roban_youyanji_install.jpg)

  

燃气灶:

  

![](./images/roban_ranqizao_size.jpg)

  

### 消毒柜

荣事达120 消毒柜(**RMB 799**):

  

![](./images/rolyastar.jpg)

  

消毒柜参数:

  

![](./images/rolyastar_parameter.jpg)

  

消毒柜尺寸:

  

![](./images/rolyastar_size.jpg)

  

消毒柜安装:

  

![](./images/rolyastar_install.jpg)

  
  

### 蒸箱、烤箱

老板蒸烤箱(**RMB 7000**)参数,使用两个10A三孔插座就行

  

![](./images/zhen_kao_parameter.jpg)

  

开孔尺寸

  

![](./images/zheng_size.jpg)

  

![](./images/kao_size.jpg)

  

### 室外燃气热水器

  

#### 樱花室外燃气热水器(**RMB 5500**):

  

![](./images/sakura_ranqireshuiqi.jpg)

  

热水器参数:

  

![](./images/sakura_ranqireshuiqi_parameter.jpg)

  

热水器尺寸:

  

![](./images/sakura_ranqireshuiqi_size.jpg)

  

热水器零冷水管安装示意图:

  

![](./images/sakura_ranqireshuiqi_huishuiguan.jpg)

  

热水器装箱清单:

  

![](./images/sakura_ranqireshuiqi_list.jpg)

  

### 电冰箱

  

#### 容声520L电冰箱(**RMB 3000**):

  

![](./images/rongshen_icebox_520.jpg)

  

冰箱参数:

  

![](./images/rongshen_icebox520_parameter.jpg)

  

### 洗衣机

  

#### 海尔洗衣机(**RMB 2699**):

  

![](./images/haier_washbox.jpg)

  

洗衣机参数:

  

![](./images/haier_washbox_parameter.jpg)

  

洗衣机尺寸:

  

![](./images/haier_washbox_size.jpg)

  

### 壁挂空调

  

#### 海信空调(**RMB 2000**)

  

![](./images/Hisense_AC_3500.jpg)

  

空调参数:

  

![](./images/Hisense_AC_3500_parameter.jpg)

  

空调尺寸

  

![](./images/Hisense_AC_3500_size.jpg)

  

### 电视机