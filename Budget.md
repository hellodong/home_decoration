<table>
    <tr>
        <th colspan="2">装修硬装大项预算</th>
    </tr>
    <tr>
        <td>中央空调</td>
        <td>25000</td>
    </tr>
    <tr>
        <td>柜子+吊顶</td>
        <td>95000</td>
    </tr>
    <tr>
        <td>水电</td>
        <td>20000</td>
    </tr>
    <tr>
        <td>封窗</td>
        <td>15000</td>
    </tr>
        <tr>
        <td>刷墙</td>
        <td>25000</td>
    </tr>
    <tr>
        <td>敲墙+铲墙皮</td>
        <td>3000</td>
    </tr>
    <tr>
        <td>地砖+瓷砖+砌墙+美缝</td>
        <td>40000</td>
    </tr>
    <tr>
        <td>房间门X3</td>
        <td>1500 x 3 = 4500</td>
    </tr>
    <tr>
        <td>灯具</td>
        <td>3000</td>
    </tr>
    <tr>
        <td>燃气热水器</td>
        <td>5500</td>
    </tr>
    <tr>
        <td>燃气灶+油烟机</td>
        <td>4000</td>
    </tr>
    <tr>
        <td>蒸烤箱体</td>
        <td>7000</td>
    </tr>
    <tr>
        <td>消毒柜</td>
        <td>800</td>
    </tr>
    <tr>
        <td>单槽+龙头</td>
        <td>600</td>
    </tr>
   <tr>
        <td>换气</td>
        <td>160</td>
    </tr>
    <tr>
        <td>花洒 x2</td>
        <td>800 x 2 = 1600</td>
    </tr>
    <tr>
        <td>浴室门 x2</td>
        <td>800 x 2 = 1600</td>
    </tr>
    <tr>
        <td>浴霸 x2</td>
        <td>300 x 2 = 600</td>
    </tr>
      <tr>
        <td>花洒</td>
        <td>700 x2 = 1400 </td>
    </tr>
    <tr>
        <td>卫生间玻璃门</td>
        <td>1600</td>
    </tr>
    <tr>
        <td>马桶 x2</td>
        <td>800 x 2 = 1600</td>
    </tr>
        <tr>
        <td>洗漱一体盆</td>
        <td>1200 x 2=2400</td>
    </tr>
    <tr>
        <td>阳台洗衣平台</td>
        <td>2000</td>
    </tr>
    <tr>
        <td>硬装总计</td>
        <td>260000</td>
    </tr>
</table>


<table>
    <tr>
        <th colspan="2">装修软装预算</th>
    </tr>
    <tr>
        <td>洗衣机</td>
        <td>2600</td>
    </tr>
    <tr>
        <td>壁挂空调</td>
        <td>2500</td>
    </tr>
    <tr>
        <td>冰箱</td>
        <td>4500</td>
    </tr>
   <tr>
        <td>电视机65寸</td>
        <td>3000</td>
    </tr>
    <tr>
        <td>窗帘</td>
        <td>5000</td>
    </tr>
    <tr>
        <td>餐桌</td>
        <td>2500</td>
    </tr>
    <tr>
        <td>沙发</td>
        <td>4500</td>
    </tr>
    <tr>
        <td>1.8米大床</td>
        <td>5000</td>
    </tr>
    <tr>
        <td>1.5米床</td>
        <td>2600x2=5200</td>
    </tr>
    <tr>
        <td>儿童房课桌椅</td>
        <td>900</td>
    </tr>
    <tr>
        <td>床头柜x3</td>
        <td>500x3=1500</td>
    </tr>
    <tr>
        <td>软装总计</td>
        <td>37000</td>
    </tr>
<table>