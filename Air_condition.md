### 空间大小功率匹配
<table>
    <tr>
        <th colspan="3" align="center">空调匹配</th>
    </tr>
    <tr>
        <td>客厅</td>
        <td>5.965 X 4.31 = 25.71</td>
        <td>3匹</td>
    </tr>
    <tr>
        <td>主卧</td>
        <td>3.26 X 4.36 = 14.21</td>
        <td>2匹</td>
    </tr>
    <tr>
        <td>小南卧</td>
        <td> 2.85 X 3.45 = 9.83</td>
        <td>1.5匹</td>
    </tr>
    <tr>
        <td>北卧</td>
        <td>2.56 X 3.06 = 7.83</td>
        <td>壁挂 1.5匹</td>
    </tr>
</table>

## 美的中央空调各参数
### 理想家3 内外机
![](./images/LX_air_condition.jpg)
![](./images/LX_air_condition_inner.png)

### 领航者3 外机
![](./images/LH_air_condition.jpg)